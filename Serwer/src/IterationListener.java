/**
 * Created by Bartek on 2017-03-14.
 */
@FunctionalInterface
public interface IterationListener {
    void handle(int var1);

}
