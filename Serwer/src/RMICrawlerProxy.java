import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 * Created by Bartek on 2017-05-14.
 */
public class RMICrawlerProxy extends UnicastRemoteObject implements RmiInterferace{

    Crawler craweler;

    public RMICrawlerProxy() throws RemoteException {
        craweler = new Crawler();


        craweler.addIterationStartedListener((i)->{System.out.println("Numer " +i);});
        craweler.addToAddStudentsListnerList((student)->{
            String status = craweler.isAdd(student);
            if(status == "ADDED") {
                this.sendMessages("ADDED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
            }

        });
        craweler.addToDeleteStudentsListnerList((student)->{
            String status = craweler.isRemove(student);
            if(status == "REMOVED") {
                this.sendMessages("REMOVED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());

            }

        });
        craweler.addToModifyStudentsListnerList((student)->{
            String status = craweler.isModify(student);
            if(status == "MODIFY") {
                this.sendMessages("MODIFIED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
            }

        });
    }

    @Override
    public String isAdd(Student student) throws IOException, RemoteException {
        return  craweler.isAdd(student);
    }

    @Override
    public String isRemove(Student student) throws IOException, RemoteException {
        return craweler.isRemove(student);
    }

    @Override
    public String isModify(Student student) throws IOException, RemoteException {
        return craweler.isModify(student);
    }

    @Override
    public void pobranieUrl() throws NoInputExpection, RemoteException {
      craweler.pobranieUrl();
    }

    @Override
    public void addToAddStudentsListnerList(AddStudentListner listner) throws RemoteException {
        craweler.addToAddStudentsListnerList(listner);
    }

    @Override
    public void removeToAddStudentsListnerList(AddStudentListner listner) throws RemoteException {
        craweler.removeToAddStudentsListnerList(listner);
    }

    @Override
    public void addToDeleteStudentsListnerList(DeleteStudentListner listner) throws RemoteException {
        craweler.addToDeleteStudentsListnerList(listner);
    }

    @Override
    public void removeToDeleteStudentsListnerList(DeleteStudentListner listner) throws RemoteException {
        craweler.removeToDeleteStudentsListnerList(listner);
    }

    @Override
    public void addToModifyStudentsListnerList(ModifyStudentListner listner) throws RemoteException {
        craweler.addToModifyStudentsListnerList(listner);
    }

    @Override
    public void removeToModifyStudentsListnerList(ModifyStudentListner listner) throws RemoteException {
        craweler.removeToModifyStudentsListnerList(listner);
    }

    @Override
    public void addIterationStartedListener(IterationListener listener) throws RemoteException {
        craweler.addIterationStartedListener(listener);
    }

    @Override
    public void removeIterationStartedListener(IterationListener listener) throws RemoteException {
        craweler.removeIterationStartedListener(listener);
    }

    @Override
    public void Run() throws InterruptedException, IOException, RemoteException {
        craweler.Run();
    }

    @Override
    public List<Student> extractStudents(Crawler.OrderMode mode) throws RemoteException {
        return craweler.extractStudents(mode);
    }

    @Override
    public double extractMark(Crawler.ExtremumMode mode) throws RemoteException {
        return craweler.extractMark(mode);
    }

    @Override
    public int extractAge(Crawler.ExtremumMode mode) throws RemoteException {
        return extractAge(mode);
    }





    private MessageEvent event = null;
    @Override
    public void setMessageEvent(MessageEvent var1) throws RemoteException {
        event = var1;
    }

    @Override
    public void sendMessages( String status) throws RemoteException {
        if(this.event !=null)
        {
            this.event.messageSended(status);
        }
    }

    @Override
     public int Add(int n1,int n2) throws RemoteException
    {
        return n1+n2;
    }
}
