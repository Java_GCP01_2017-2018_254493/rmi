import java.io.IOException;

/**
 * Created by Bartek on 2017-03-13.
 */
//@FunctionalInterface
public interface DeleteStudentListner {
    void handle(Student var1) throws IOException;

}
