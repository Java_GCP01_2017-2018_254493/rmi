
import javafx.scene.layout.AnchorPane;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 * Created by Bartek on 2017-03-21.
 */
public class CustomBarChart extends AnchorPane {

    public int two,three,threePlus,four,fourPlus,five;
    XYChart.Series markSeries = new XYChart.Series();
    CategoryAxis xAxis = new CategoryAxis();
    NumberAxis yAxis = new NumberAxis();
    BarChart<String,Number> bc = new BarChart<String,Number>(xAxis,yAxis);

public void getMark (Student student)
{
    if(student.getMark()==2.0)
    {
        ++two;
    }
    else if(student.getMark()==3.0)
    {
        ++three;
    }
    else if(student.getMark()==3.5)
    {
        ++threePlus;
    }
    else if(student.getMark()==4.0)
    {
        ++four;
    }
    else if(student.getMark()==4.5)
    {
        ++fourPlus;
    }
    else if(student.getMark()==5.0)
    {
        ++five;
    }
}



     public void createBarChart()
    {
        bc.setTitle("Distribution of marks");
        xAxis.setLabel("Marks");
        yAxis.setLabel("Count");



markSeries.setName("Marks");
        markSeries.getData().add(new XYChart.Data("2.0", two));
        markSeries.getData().add(new XYChart.Data("3.0", three));
        markSeries.getData().add(new XYChart.Data("3.5", threePlus));
        markSeries.getData().add(new XYChart.Data("4.0", four));
        markSeries.getData().add(new XYChart.Data("4.5", fourPlus));
        markSeries.getData().add(new XYChart.Data("5", five));
        bc.getData().addAll(markSeries);











        AnchorPane.setTopAnchor(bc, 2.0);
        AnchorPane.setLeftAnchor(bc, 2.0);
        AnchorPane.setRightAnchor(bc, 2.0);
        getChildren().add(bc);
    }

}
