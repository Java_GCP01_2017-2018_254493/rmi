import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import  java.util.*;
import java.util.Scanner;
import java.net.URL;

//import static Crawler.ExtremumMode.*;
import static java.util.Collections.*;

/**
 * Created by Bartek on 2017-03-12.
 */


public class Crawler implements Serializable {
   public Set<Student> parseStudentList;
   public Set<Student> studentList;
    public List<Student> myList2;
   public List<Student> myList1;

String url;
    private List<AddStudentListner> addStudentsListnerList;
    private List<DeleteStudentListner> deleteStudentsListnerList ;
    private List<ModifyStudentListner> modifyStudentListnerList;
    private List<IterationListener> iterationStartedListenersList;

public Crawler() //konstruktor
{
studentList=new HashSet<Student>();
    addStudentsListnerList= new ArrayList();// HashSet jak by lista tylko że dodaje tylko jak nie ma takiego samego elementu
    iterationStartedListenersList = new ArrayList();
    deleteStudentsListnerList= new ArrayList();
    modifyStudentListnerList = new ArrayList();
}

public String isAdd(Student student) throws IOException {
    if (!(parseStudentList.size() == studentList.size())) {
        if (studentList.contains(student)) {
            return "";
        } else {
            studentList.add(student);
            return "ADDED";
        }
    }
    return "";
}
    public String isRemove(Student student) throws IOException {
        if(parseStudentList.contains(student))
        {
            return "";
        }
        else
        {

studentList=parseStudentList;
            return "REMOVED";
        }
    }

    public String isModify(Student student) throws IOException {
       if(parseStudentList.size()==studentList.size()) {
           if (studentList.contains(student)) {
               return "";
           } else {
               studentList=parseStudentList;
               return "MODIFY";
           }
       }
        return "";
    }

public void pobranieUrl() throws NoInputExpection
{  System.out.println("Podaj uri");

    Scanner read= new Scanner(System.in); //obiekt do odebrania danych od użytkownika
    url = read.nextLine();
    try {
       //url = read.nextLine();
        URL newurl=new URL (url);
    }catch(MalformedURLException e)
    {

        throw new NoInputExpection();
    }

}


//Eventy
    public synchronized  void addToAddStudentsListnerList(AddStudentListner listner)
    {
        addStudentsListnerList.add(listner);
    }
    public synchronized  void removeToAddStudentsListnerList(AddStudentListner listner )
    {
        addStudentsListnerList.remove(listner);
    }

    public synchronized  void addToDeleteStudentsListnerList(DeleteStudentListner listner)
    {
        deleteStudentsListnerList.add(listner);
    }
    public synchronized  void removeToDeleteStudentsListnerList(DeleteStudentListner listner )
    {
        deleteStudentsListnerList.remove(listner);
    }
    public synchronized  void addToModifyStudentsListnerList(ModifyStudentListner listner)
    {
        modifyStudentListnerList.add(listner);
    }
    public synchronized  void removeToModifyStudentsListnerList(ModifyStudentListner listner )
    {
        modifyStudentListnerList.remove(listner);
    }

    public void addIterationStartedListener(IterationListener listener) {
        this.iterationStartedListenersList.add(listener);
    }

    public void removeIterationStartedListener(IterationListener listener) {
        this.iterationStartedListenersList.remove(listener);
    }

    //koniec evetnow


    public enum OrderMode
    {
        MARK,
        FIRST_NAME,
        LAST_NAME,
        AGE;
    }
    public enum ExtremumMode
    {
        MAX,
        MIN
    }


    public void Run() throws InterruptedException, IOException {

        int iteration = 1;


        while (true) {
            parseStudentList=new HashSet<Student>(StudentsParser.parse( new File( "students.txt") ));

         //   List<Student> myList = new ArrayList(studentList);

         //  sort(myList,Student.StudentAgeComparator);

         //  for( Student el : myList)
         //    System.out.println( el.getMark() + " " + el.getFirstName() + " " + el.getLastName() + " " + el.getAge() );

            for (IterationListener el : iterationStartedListenersList) {
                el.handle(iteration);
                ++iteration;
            }
            for (Student parseSt : parseStudentList) {
                for (AddStudentListner el : addStudentsListnerList) {
                    el.handle(parseSt);
                }
                for (ModifyStudentListner el :  modifyStudentListnerList) {
                    el.handle(parseSt);
                }
            }

            for (Student st:studentList) {
                for (DeleteStudentListner el : deleteStudentsListnerList) {
                    el.handle(st);
                }


            }

//          System.out.println( "AGE: <"+extractAge(ExtremumMode.MIN)+","+extractAge(ExtremumMode.MAX)+">");
//           System.out.println( "MARK: <"+extractMark(ExtremumMode.MIN)+","+extractMark(ExtremumMode.MAX)+">");


       for( Student el : extractStudents(OrderMode.MARK))
           System.out.println( el.getMark() + " " + el.getFirstName() + " " + el.getLastName() + " " + el.getAge() );

            Thread.sleep(5000);


        }
    }




   List<Student> extractStudents( OrderMode mode ) // posortowani studenci
    {
        List<Student> myList = new ArrayList(studentList);
        switch (mode)
        {
            case MARK:
                sort(myList,Student.StudentMarkComparator);
                break;
            case FIRST_NAME:
                sort(myList,Student.StudentFirstNameComparator);
                break;
            case LAST_NAME:
                sort(myList,Student.StudentLastNameComparator);
                break;
            case AGE:
                sort(myList,Student.StudentAgeComparator);
                break;
        }
        return myList;
    }
  double extractMark( ExtremumMode mode ) // maksymalna lub minimalna ocena
  {
      Student s = new Student();
       myList1 = new ArrayList(studentList);
      switch (mode)
      {
          case MAX:
              s=  max(myList1, Student.StudentMarkComparator);
              break;
          case MIN:
              s=  min(myList1, Student.StudentMarkComparator);
              break;
      }
      return s.getMark();
  }
int extractAge( ExtremumMode mode ) //maksymalny lub minimalny wiek
{
    Student s = new Student();
    myList2 = new ArrayList(studentList);
    switch (mode)
    {
        case MAX:
            s=  max(myList2, Student.StudentAgeComparator);
            break;
        case MIN:
            s=  min(myList2, Student.StudentAgeComparator);
            break;
    }
    return s.getAge();
}

}




