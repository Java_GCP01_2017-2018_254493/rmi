import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Bartek on 2017-05-15.
 */
public interface MessageEvent extends Serializable, Remote {
    void messageSended( String message) throws RemoteException;
}
