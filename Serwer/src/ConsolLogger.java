/**
 * Created by Bartek on 2017-03-18.
 */
public class ConsolLogger implements Logger {
    @Override
    public void log(String status, Student student) {
        if(status == "ADDED") {
            System.out.println("ADDED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }

        if(status == "REMOVED") {
            System.out.println("REMOVED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }
        if(status == "MODIFY") {
            System.out.println("MODIFIED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }

    }
}
