import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

//import static Crawler.ExtremumMode.MIN;

public class Program
{

	public static void main( String[] args ) throws IOException, InterruptedException {

		Crawler proba = new Crawler();

		final Logger[] loggers = new Logger[]
				{


						new ConsolLogger()
				};


		try {
			proba.pobranieUrl();
		}
		catch(NoInputExpection e)
		{
			System.out.println(e.getMessage());
			//return;
		}

		proba.addIterationStartedListener((i)->{System.out.println("Numer " +i);});
		proba.addToAddStudentsListnerList((student)->{
			String status = proba.isAdd(student);
			for( Logger el : loggers )
				el.log(status, student );

		});
        proba.addToDeleteStudentsListnerList((student)->{
        	String status = proba.isRemove(student);
        	for( Logger el : loggers )
				el.log(status, student );
        });
		proba.addToModifyStudentsListnerList((student)->{
			String status = proba.isModify(student);
			for( Logger el : loggers )
				el.log(status, student );
		});
      proba.Run();
	}

	//(()->)



}
