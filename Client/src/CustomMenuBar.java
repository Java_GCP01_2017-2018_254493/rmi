import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;




/**
 * Created by Bartek on 2017-03-21.
 */
public class CustomMenuBar extends javafx.scene.control.MenuBar {
    public Menu program = new Menu("Program");

     public Menu about = new Menu("About");
MenuItem aboutIteam = new MenuItem("About");
MenuItem closeIteam = new MenuItem("Close Ctrl+C");



   public void add(Stage primaryStage)
   {

       primaryStage.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
           if (KeyCode.CONTROL  == event.getCode()) {
               primaryStage.close();
           }
       });


program.getItems().add(closeIteam);
       about.getItems().add(aboutIteam);


       closeIteam.setOnAction(new EventHandler<ActionEvent>() {
           @Override public void handle(ActionEvent event) {

primaryStage.close();

           }

           }



       );
       aboutIteam.setOnAction(

               new EventHandler<ActionEvent>() {
                              @Override public void handle(ActionEvent event) {
                                final Stage dialog = new Stage();
                                  dialog.initModality(Modality.APPLICATION_MODAL);
                                   dialog.initOwner(primaryStage);
                                  VBox dialogVbox = new VBox(50);
                                   dialogVbox.getChildren().add(new Text("Author: Bartłomiej Leja"));
                                   Scene dialogScene = new Scene(dialogVbox, 300, 200);
                                   dialog.setScene(dialogScene);
                                   dialog.show();
                               }
                          });

       getMenus().add(program);
       getMenus().add(about);

   }
}
