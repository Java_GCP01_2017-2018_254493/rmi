import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Bartek on 2017-05-14.
 */
public class Serwer {

    public static void main(String[] args)
    {
        try{
            Registry reg = LocateRegistry.createRegistry(1099);
            RMICrawlerProxy rcp =new RMICrawlerProxy();
            reg.rebind("myCraweler",rcp);
            System.out.println("Server is ready");

        }
        catch(Exception ex)
        {

        }
    }
}
