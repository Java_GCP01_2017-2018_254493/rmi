import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by Bartek on 2017-05-14.
 */
public interface RmiInterferace extends Remote {
    String isAdd(Student student) throws IOException, RemoteException;

    String isRemove(Student student) throws IOException, RemoteException;

    String isModify(Student student) throws IOException, RemoteException;

    void pobranieUrl() throws NoInputExpection, RemoteException;

    void addToAddStudentsListnerList(AddStudentListner listner) throws RemoteException;

    void removeToAddStudentsListnerList(AddStudentListner listner) throws RemoteException;

    void addToDeleteStudentsListnerList(DeleteStudentListner listner) throws RemoteException;

    void removeToDeleteStudentsListnerList(DeleteStudentListner listner) throws RemoteException;

    void addToModifyStudentsListnerList(ModifyStudentListner listner) throws RemoteException;

    void removeToModifyStudentsListnerList(ModifyStudentListner listner) throws RemoteException;

    void addIterationStartedListener(IterationListener listener) throws RemoteException;

    void removeIterationStartedListener(IterationListener listener) throws RemoteException;

    void Run() throws InterruptedException, IOException, RemoteException;

    List<Student> extractStudents(Crawler.OrderMode mode ) throws RemoteException;

    double extractMark( Crawler.ExtremumMode mode )throws RemoteException;

    int extractAge( Crawler.ExtremumMode mode )throws RemoteException;

    void setMessageEvent(MessageEvent var1) throws RemoteException;

    void sendMessages( String message) throws RemoteException;

    int Add(int n1,int n2) throws RemoteException;

}
