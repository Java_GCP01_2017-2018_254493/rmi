
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.awt.dnd.Autoscroll;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Bartek on 2017-03-21.
 */
public class CustomTableView extends AnchorPane {

private TableView<Student> table= new TableView<>();
private TableView<String> logTable = new TableView<>(); //nie uzywana ale wole nie kasowac bo i tak do tego nie wracam
private ObservableList<Student> students= FXCollections.observableArrayList();
//
   private ListView<String> logList = new ListView<>();

public void addLogTab()
{

    AnchorPane.setTopAnchor(logList, 2.0);
   AnchorPane.setLeftAnchor(logList, 2.0);
    AnchorPane.setRightAnchor(logList, 2.0);
    getChildren().add(logList);
}

public void getLogs(String message)
{
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern(" HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
logList.getItems().add(dtf.format(now)+ message);

}
public void getStudents(Student student)
{
    students.add(student);
    table.setItems(students);
}

public void  deleteStudent(Student student)
{
    students.remove(student);
    table.setItems(students);
}

public void modifyStudend() throws IOException {
    Set<Student> tmp= new HashSet<Student>(StudentsParser.parse(new File( "students.txt" )));
    students= FXCollections.observableArrayList();
    for (Student e:tmp) {
        students.add(e);

    }
    table.setItems(students);
}

public void  addStudentsTabel()
{
    TableColumn<Student,Double> markColumn = new TableColumn<>("Mark");
    markColumn.setMinWidth(200);
    markColumn.setCellValueFactory(new PropertyValueFactory<>("mark"));

    TableColumn<Student,String> firstNameColumn = new TableColumn<>("First Name");
    firstNameColumn.setMinWidth(200);
    firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

    TableColumn<Student,String> lastNameColumn = new TableColumn<>("Last Name");
    lastNameColumn.setMinWidth(200);
    lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

    TableColumn<Student,Integer> ageNameColumn = new TableColumn<>("Age");
    ageNameColumn.setMinWidth(200);
    ageNameColumn.setCellValueFactory(new PropertyValueFactory<>("age"));


    table.getColumns().add(markColumn);
    table.getColumns().add(firstNameColumn);
    table.getColumns().add(lastNameColumn);
    table.getColumns().add(ageNameColumn);
    AnchorPane.setTopAnchor(table, 2.0);
    AnchorPane.setLeftAnchor(table, 2.0);
    AnchorPane.setRightAnchor(table, 2.0);

    getChildren().add(table);
}
}
