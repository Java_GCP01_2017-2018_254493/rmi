import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Comparator;
public class Student implements Comparable<Student>, Serializable
{
	@Override
	public int compareTo(Student studentToCompare)  {

		return Double.compare(studentToCompare.getMark(),this.getMark());
	}
	//@sortowanie po imieniu
	public static Comparator<Student> StudentFirstNameComparator
			= new Comparator<Student>() {

		public int compare(Student s1, Student s2) {

			String studentName1 = s1.getFirstName().toUpperCase();
			String studentName2 = s2.getFirstName().toUpperCase();

			//ascending order
			return studentName1.compareTo(studentName2);

			//descending order
			//return studentName2.compareTo(studentName1);
		}
	};

	//@sortowanie po nazwisku
	public static Comparator<Student> StudentLastNameComparator
			= new Comparator<Student>() {

		public int compare(Student s1, Student s2) {

			String studentName1 = s1.getLastName().toUpperCase();
			String studentName2 = s2.getLastName().toUpperCase();

			//ascending order
			return studentName1.compareTo(studentName2);

			//descending order
			//return studentName2.compareTo(studentName1);
		}
	};
	//@sortowanie po wieku
	public static Comparator<Student> StudentAgeComparator
			= new Comparator<Student>() {

		public int compare(Student s1, Student s2) {


			//ascending order
			return Double.compare(s1.getAge(),s2.getAge());

			//descending order
			// return Double.compare(s2.getAge(),s1.getAge());
		}

	};
	//@sortowanie po ocenie
	public static Comparator<Student> StudentMarkComparator
			= new Comparator<Student>() {

		public int compare(Student s1, Student s2) {


			//ascending order
			return Double.compare(s1.getMark(),s2.getMark());

			//descending order
			// return Double.compare(s2.getAge(),s1.getAge());
		}

	};

	private double mark;
	private String firstName;
	private String lastName;
	private int age;
	
	public double getMark()
	{
		return this.mark;
	}
	
	public void setMark( double mark )
	{
		this.mark = mark;
	}
	
	public String getFirstName()
	{
		return this.firstName;
	}
	
	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public void setAge( int age )
	{
		this.age = age;
	}




	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		
		result = prime * result + age;
		result = prime * result + ( ( firstName == null ) ? 0 : firstName.hashCode() );
		result = prime * result + ( ( lastName == null ) ? 0 : lastName.hashCode() );
		
		long temp;
		temp = Double.doubleToLongBits( mark );
		result = prime * result + (int) ( temp ^ ( temp >>> 32 ) );
		
		return result;
	}
	
	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		
		if ( obj == null )
			return false;
		
		if ( getClass() != obj.getClass() )
			return false;
		
		Student other = (Student) obj;
		
		if ( age != other.age )
			return false;
		
		if ( firstName == null )
		{
			if ( other.firstName != null )
				return false;
		}
		else 
			if ( !firstName.equals( other.firstName ) )
				return false;
		
		if ( lastName == null )
		{
			if ( other.lastName != null )
				return false;
		}
		else 
			if ( !lastName.equals( other.lastName ) )
				return false;
		
		if ( Double.doubleToLongBits( mark ) != Double.doubleToLongBits( other.mark ) )
			return false;
		
		return true;
	}
	
}
