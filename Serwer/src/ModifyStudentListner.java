import java.io.IOException;

/**
 * Created by Bartek on 2017-03-13.
 */
public interface ModifyStudentListner {
    void handle(Student var1) throws IOException;
}
