import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Bartek on 2017-05-14.
 */
public class Client extends Thread       {

    CustomTableView _tableStudents;
    CustomTableView _tableLog;
    private CustomBarChart _barChart;

//    public static void main(String[] args)


    public Client(CustomTableView logTable)
    {
        //_tableStudents = table;
        _tableLog=logTable;
      //  _barChart=barChart;

    }
public void run()
{
        try{
            Registry reg = LocateRegistry.getRegistry("localhost",1099);

            RmiInterferace ri = (RmiInterferace) reg.lookup("myCraweler");
        // System.out.println("1 + 1 =" + ri.Add(1,1));
           ri.setMessageEvent(new CustomMessageEvent());

            ri.Run();




        }catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private class CustomMessageEvent extends UnicastRemoteObject implements MessageEvent
    {
        private static final long serialVersionUID = 6738492455439057283L;

        protected CustomMessageEvent() throws RemoteException
        {
            super();
        }

        @Override
        public void messageSended( String message )
        {
            System.out.printf( "Server message: %s\n", message );
            _tableLog.getLogs(message);
        }
    }


}
