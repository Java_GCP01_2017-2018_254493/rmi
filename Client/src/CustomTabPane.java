import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.control.TabPane;

import java.awt.*;

/**
 * Created by Bartek on 2017-03-21.
 */
public class CustomTabPane extends AnchorPane {

    CustomMenuBar menuBar  =new CustomMenuBar();
    Tab students = new Tab();
    Tab log = new Tab();
    Tab histogram = new Tab();
    TabPane tabPane = new TabPane();
    CustomTableView customTableView = new CustomTableView();
CustomTableView logTableView = new CustomTableView();
CustomBarChart customBarChart=new CustomBarChart();

    public CustomTabPane() {

        super();
    }

    @Override
    public ObservableList<Node> getChildren() {

        return super.getChildren();
    }


    public void addComponents(Stage primaryStage)
    {

        customTableView.addStudentsTabel();
        customBarChart.createBarChart();
        logTableView.addLogTab();
        log.setText("Log");
        log.setContent(logTableView);
        students.setText("Students");
        students.setContent(customTableView);
        histogram.setText("Histogram");
        histogram.setContent(customBarChart);
        tabPane.getTabs().add(students);
        tabPane.getTabs().add(log);
        tabPane.getTabs().add(histogram);
        menuBar.add(primaryStage);
        AnchorPane.setTopAnchor(menuBar, 2.0);
        AnchorPane.setLeftAnchor(menuBar, 2.0);
        AnchorPane.setRightAnchor(menuBar, 2.0);
        getChildren().add(menuBar);
        AnchorPane.setTopAnchor(tabPane, 30.0);
        AnchorPane.setLeftAnchor(tabPane, 2.0);
        AnchorPane.setRightAnchor(tabPane, 2.0);


        getChildren().add(tabPane);
    }
}
